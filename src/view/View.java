package view;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Scanner;

import controller.Controller;
import data_structures.List;

public class View {

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();

			int option = sc.nextInt();
			int number = 0;

			switch(option)
			{
				case 1:
					try {
						Controller.cargar();
					} catch (IOException e) {
						System.out.println("No se pudo cargar el archivo");
						e.printStackTrace();
					}
					break;
					
				case 2:
					try {
						Controller.cargarGrafo();
					} catch (Exception e) {
						System.out.println("No se pudo cargar el grafo");
						e.printStackTrace();
					}
					break;
					
				case 4:
					fin = true;
					sc.close();
					break;
					
				default:
					System.out.println("La opción no es válida");
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 4----------------------");
		System.out.println("1. Cargar datos");
		System.out.println("2. Cargar grafo");
		System.out.println("4. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");
	}
	
	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @param hora hora en formato hh:mm:ss con hh para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	public static LocalDateTime convertirFecha_Hora_LDT(String fecha, String hora)
	{
		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[0]);
		int dia = Integer.parseInt(datosFecha[1]);
		int horas = Integer.parseInt(datosHora[0]);
		int minutos = Integer.parseInt(datosHora[1]);
		int segundos;
		try {
			//TODO Se puede reducir?
			segundos = Integer.parseInt(datosHora[2]);
		} catch (ArrayIndexOutOfBoundsException e) {
			segundos = 0;
		}

		return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
	}
	
}
