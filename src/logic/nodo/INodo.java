package logic.nodo;

public interface INodo {

	public double darLatitud();
	public double darLongitud();
	public int darId();
	
}
