package logic.nodo;

public abstract class Nodo implements INodo {

	//-------------------------------------------------------------
	//Atributos
	//-------------------------------------------------------------

	protected double latitud;

	protected double longitud;

	protected int id;

	//-------------------------------------------------------------
	//Constructor
	//-------------------------------------------------------------

	public Nodo(double latitud, double longitud, int id) {
		this.latitud = latitud;
		this.longitud = longitud;
		this.id = id;
	}

	//-------------------------------------------------------------
	//Métodos
	//-------------------------------------------------------------

	@Override
	public double darLatitud() {
		return latitud;
	}

	@Override
	public double darLongitud() {
		return longitud;
	}

	@Override
	public int darId() {
		return id;
	}
	
	public abstract String toString();
}
