package logic.nodo;

public class Interseccion extends Nodo {

	public Interseccion(double latitud, double longitud, int id) {
		super(latitud, longitud, id);
	}

	@Override
	public String toString() {
		return String.valueOf(id + ";" + latitud + ";" + longitud);
	}
}
