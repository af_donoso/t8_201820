package logic.nodo;

import java.time.LocalDateTime;

public class Station extends Nodo {

	//-------------------------------------------------------------
	//Atributos
	//-------------------------------------------------------------

	private String name;

	private String city;

	private int dpCapacity;

	private LocalDateTime date;

	//-------------------------------------------------------------
	//Atributos
	//-------------------------------------------------------------

	public Station(double latitud, double longitud, int id, String name, String city, int dpCapacity, 
			LocalDateTime date) {
		super(latitud, longitud, id);
		this.name = name;
		this.city = city;
		this.dpCapacity = dpCapacity;
		this.date = date;
	}

	//-------------------------------------------------------------
	//Atributos
	//-------------------------------------------------------------

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @return the dpCapacity
	 */
	public int getDpCapacity() {
		return dpCapacity;
	}

	/**
	 * @return the date
	 */
	public LocalDateTime getDate() {
		return date;
	}

	@Override
	public String toString() {
		return String.valueOf(super.id + ";" + super.latitud + ";" + super.longitud + ";" + name + ";" 
				+ city + ";" + dpCapacity + ";" + date.toString());
	}
	
	
}
