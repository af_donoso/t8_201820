package logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.opencsv.CSVReader;

import data_structures.Arco;
import data_structures.Graph;
import data_structures.List;
import logic.nodo.Interseccion;
import logic.nodo.Nodo;
import logic.nodo.Station;
import view.View;

public class Manager {

	public final static String RUTA_CALLES = "./data/Adjacency_list_of_Chicago_Street_Lines.txt";
	public final static String RUTA_INTERSECCIONES = "./data/Nodes_of_Chicago_Street_Lines.txt";
	public final static String STATIONS_FILEQ3_Q4 = "./data/Divvy_Stations_2017_Q3Q4.csv";

	private Graph<Integer, Nodo, Calle> grafo = new Graph<>();

	public void cargar() throws IOException {
		int numVertices;
		int numArcos;

		/**
		 * Intersecciones y calles
		 */
		BufferedReader reader = new BufferedReader(new FileReader(new File(RUTA_INTERSECCIONES)));
		String linea;
		String datos[];

		while((linea = reader.readLine()) != null) {
			datos = linea.split(",");

			int id = Integer.valueOf(datos[0]);
			double longitud = Double.valueOf(datos[1]);
			double latitud = Double.valueOf(datos[2]);

			Interseccion interseccion = new Interseccion(latitud, longitud, id);
			grafo.addVertex(id, interseccion);
		}

		reader = new BufferedReader(new FileReader(new File(RUTA_CALLES)));

		while((linea = reader.readLine()) != null) {
			datos = linea.split(" ");

			int vertice1 = Integer.valueOf(datos[0]);
			Interseccion interseccion1 = (Interseccion) grafo.getInfoVertex(vertice1);

			for(int i = 1; i < datos.length; i++) {
				int verticeAdj = Integer.valueOf(datos[i]);
				Interseccion interseccion2 = (Interseccion) grafo.getInfoVertex(verticeAdj);

				grafo.addEdge(vertice1, verticeAdj, new Calle(Haversine.distance(interseccion1.darLatitud(), 
						interseccion1.darLongitud(), interseccion2.darLatitud(), interseccion2.darLongitud())));
			}
		}

		reader.close();
		numVertices = grafo.V();
		numArcos = grafo.E();
		System.out.println("Número vertices (Intersección): " + numVertices);
		System.out.println("Número arcos: " + numArcos);

		/**
		 * Estaciones
		 */

		CSVReader csvReader = new CSVReader(new FileReader(STATIONS_FILEQ3_Q4));
		String[] nextLine;
		csvReader.readNext();

		while ((nextLine = csvReader.readNext()) != null) {
			LocalDateTime time = View.convertirFecha_Hora_LDT(nextLine[6].split(" ")[0], nextLine[6].split(" ")[1]);

			Station station = new Station(Double.valueOf(nextLine[3]), Double.valueOf(nextLine[4]), 
					Integer.valueOf(nextLine[0]), nextLine[1], nextLine[2], Integer.valueOf(nextLine[5]), 
					time);

			grafo.addVertex(grafo.V(), station);

			double menorDistancia = Double.MAX_VALUE;
			Interseccion interseccionAConectar=null;

			Iterator<Integer> iter = grafo.vertices();
			while(iter.hasNext())
			{	
				Integer act = iter.next();

				if(grafo.getInfoVertex(act) instanceof Interseccion)
				{
					Interseccion actual =  (Interseccion) grafo.getInfoVertex(act);
					double distanciaActual = Haversine.distance(station.darLatitud(), station.darLongitud(), actual.darLatitud(), actual.darLongitud());
					if(menorDistancia>distanciaActual)
					{
						menorDistancia = distanciaActual;
						interseccionAConectar=actual;
					}
				}
			}

			Calle conexion = new Calle(menorDistancia);
			grafo.addEdge(grafo.V() - 1, interseccionAConectar.darId(), conexion);
		}

		System.out.println("Número vertices (Estacion): " + (grafo.V() - numVertices));
		System.out.println("Número arcos (Mixtos): " + (grafo.E() - numArcos));
		csvReader.close();

		guardarGrafo();
		
		dibujarGrafo();
	}

	@SuppressWarnings("unchecked")
	private void guardarGrafo() throws IOException {
		JSONObject obj = new JSONObject();
		JSONArray intersecciones = new JSONArray();
		JSONArray estaciones = new JSONArray();
		JSONArray arcos = new JSONArray();

		Iterator<Integer> iter = grafo.vertices();
		while(iter.hasNext()) {
			int id = iter.next();
			Nodo nodo = grafo.getInfoVertex(id);

			if(nodo instanceof Interseccion) {
				intersecciones.add(nodo.toString());
			} else {
				estaciones.add(id + ";" + nodo.toString());
			}
		}

		Iterator<Arco<Integer, Calle>> iterCalles = grafo.arcos().iterator();
		while(iterCalles.hasNext()) {
			Arco<Integer, Calle> arco = iterCalles.next();
			String infoArco = String.valueOf(arco.darVertice1() + ";" + arco.darVertice2() + ";"
					+ arco.darInfo().toString());

			arcos.add(infoArco);
		}

		obj.put("Intersecciones", intersecciones);
		obj.put("Estaciones", estaciones);
		obj.put("Arcos", arcos);

		FileWriter fw = new FileWriter(new File("./data/mallaChicago.json"), false);
		fw.write(obj.toJSONString());
		fw.close();

	}

	public void cargarGrafo() throws FileNotFoundException, IOException, ParseException {
		grafo = new Graph<>();
		int numArcos = 0;
		int numVertices = 0;

		JSONParser parser = new JSONParser();

		JSONObject obj = (JSONObject) parser.parse(new FileReader("./data/mallaChicago.json"));

		JSONArray array = (JSONArray) obj.get("Estaciones");
		for(Object o: array) {
			String object = String.valueOf(o);
			String[] params = object.split(";");

			Station estacion = new Station(Double.valueOf(params[2]), Double.valueOf(params[3]), 
					Integer.valueOf(params[1]), params[4], params[5], Integer.valueOf(params[6]), 
					LocalDateTime.parse(params[7]));
			System.out.println(params[0]);
			grafo.addVertex(Integer.valueOf(params[0]), estacion);
			numVertices ++;
		}

		array = (JSONArray) obj.get("Intersecciones");
		for(Object o: array) {
			String object = String.valueOf(o);
			String[] params = object.split(";");

			Interseccion interseccion = new Interseccion(Double.valueOf(params[1]), Double.valueOf(params[2]), 
					Integer.valueOf(params[0]));
			grafo.addVertex(interseccion.darId(), interseccion);
		}

		array = (JSONArray) obj.get("Arcos");
		for(Object o: array) {
			String object = String.valueOf(o);
			String[] params = object.split(";");

			Calle calle = new Calle(Double.valueOf(params[2]));
			grafo.addEdge(Integer.valueOf(params[0]), Integer.valueOf(params[1]), calle);
			Nodo nodo1 = grafo.getInfoVertex(Integer.valueOf(params[0]));

			if(nodo1 instanceof Station) {
				numArcos ++;
			}
		}

		System.out.println("Número vertices (Interseccion): " + (grafo.V() - numVertices));
		System.out.println("Número vertices (Estacion): " + numVertices);
		System.out.println("Número arcos (Interseccion): " + (grafo.E() - numArcos));
		System.out.println("Número arcos (Mixtos): " + numArcos);
	}

	public void dibujarGrafo() throws IOException
	{
		File archivo = new File("./data/grafo.html");
		PrintWriter pw = new PrintWriter(archivo);

		pw.println("<!DOCTYPE html>\r\n" + 
				"<html>\r\n" + 
				"<head>\r\n" + 
				"<meta charset=utf-8 />\r\n" + 
				"<title>A simple map</title>\r\n" + 
				"<meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />\r\n" + 
				"<script src='https://api.mapbox.com/mapbox.js/v3.1.1/mapbox.js'></script>\r\n" + 
				"<link href='https://api.mapbox.com/mapbox.js/v3.1.1/mapbox.css' rel='stylesheet' />\r\n" + 
				"<style>\r\n" + 
				"  body { margin:0; padding:0; }\r\n" + 
				"  #map { position:absolute; top:0; bottom:0; width:100%; }\r\n" + 
				"</style>\r\n" + 
				"</head>\r\n" + 
				"<body>\r\n" + 
				"<div id='map'></div>\r\n" + 
				"<script>\r\n" + 
				"L.mapbox.accessToken = 'pk.eyJ1IjoiYW5kcmVzLWRvbm9zbyIsImEiOiJjam9kdzhwbmwyczRjM3BwYndmZXJlY2x3In0.aUyn6b2ANPBzOg9CRReG_g';\r\n" + 
				"var map = L.mapbox.map('map', 'mapbox.streets')\r\n" + 
				"    .setView([41.943139372530865, -87.68479897232211], 100);");

		Iterator<Arco<Integer, Calle>> iter = grafo.arcos().iterator();
		while(iter.hasNext())
		{
			Arco<Integer, Calle> act = iter.next();

			Nodo nodo1 = grafo.getInfoVertex(act.darVertice1());
			Nodo nodo2 = grafo.getInfoVertex(act.darVertice2());

			double longitud1 = nodo1.darLongitud();
			double latitud1 = nodo1.darLatitud();
			double longitud2 = nodo2.darLongitud();
			double latitud2 = nodo2.darLatitud();

			pw.println("L.circle(L.latLng("+latitud1+", "+longitud1+"), 7, {\r\n" + 
					"    stroke: false,\r\n" + 
					"    fill: true,\r\n" + 
					"    fillOpacity: 1,\r\n" + 
					"    fillColor: \"#5b94c6\",\r\n" + 
					"    className: \"circle_500\"\r\n" + 
					"}).addTo(map);");

			pw.println("L.circle(L.latLng("+latitud2+", "+longitud2+"), 7, {\r\n" + 
					"    stroke: false,\r\n" + 
					"    fill: true,\r\n" + 
					"    fillOpacity: 1,\r\n" + 
					"    fillColor: \"#5b94c6\",\r\n" + 
					"    className: \"circle_500\"\r\n" + 
					"}).addTo(map);");

			pw.println("var line_point = [["+latitud1+", "+longitud1+"], ["+latitud2+","+longitud2+"]];\r\n" + 
					"    var polyline_opt = {\r\n" + 
					"      color : \"#5b94c6\"\r\n" + 
					"    }\r\n" + 
					"\r\n" + 
					"    L.polyline(line_point, polyline_opt).addTo(map);");

		}
		pw.println();

		pw.println("</script>");
		pw.println("</body>");
		pw.println("</html>");
		pw.close();

	}

}
