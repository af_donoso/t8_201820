package data_structures;

import java.util.Iterator;

import data_structures.Queue;

public class Graph <K extends Comparable<K>, V, A> implements IGraph<K, V, A>
{

	private SeparateChainingHashST<K, Vertice<K, V>> vertices;
	private SeparateChainingHashST<K, List<Arco<K,A>>> arcos;
	private int numArcos;
	private int numVertices;

	public Graph() {
		vertices = new SeparateChainingHashST<>(10000);
		arcos = new SeparateChainingHashST<>(10000);
		numArcos = 0;
		numVertices = 0;
	}

	@Override
	public int V() {
		return numVertices;
	}

	@Override
	public int E() {
		return numArcos;
	}

	@Override
	public void addVertex(K idVertex, V infoVertex) {
		Vertice<K,V> vertice = new Vertice<>(idVertex, infoVertex);
		vertices.put(idVertex, vertice);
		numVertices ++;
	}

	@Override
	public void addEdge(K idVertexIni, K idVertexFin, A infoArc) {
		List<Arco<K,A>> lista = arcos.get(idVertexIni);
		if(lista == null) {
			lista = new List<>();
		}
		
		lista.add(new Arco<>(idVertexIni, idVertexFin, infoArc));

		arcos.put(idVertexIni, lista);
		numArcos ++;
	}

	@Override
	public V getInfoVertex(K idVertex) {
		Vertice<K,V> vertice = vertices.get(idVertex);
		if(vertice != null) {
			return vertice.getValue();
		}
		
		return null;
	}

	@Override
	public void setInfoVertex(K idVertex, V infoVertex) {
		vertices.get(idVertex).setValue(infoVertex);
	}

	@Override
	public A getInfoArc(K idVertexIni, K idVertexFin) {
		List<Arco<K,A>> lista = arcos.get(idVertexIni);
		if(lista != null) {
			Arco<K,A> arco = lista.get(new Arco<>(idVertexIni, idVertexFin, null));
			
			if(arco != null) {
				return arco.darInfo();
			}
		}
		
		return null;
	}

	@Override
	public void setInfoArc(K idVertexIni, K idVertexFin, A infoArc) {
		arcos.get(idVertexIni).get(new Arco<>(idVertexIni, idVertexFin, null)).setInfo(infoArc);
	}

	@Override
	public Iterable<K> adj(K idVertex) {
		List<K> keys = new List<>();
		
		List<Arco<K,A>> lista = arcos.get(idVertex);
		Iterator<Arco<K,A>> iter = lista.iterator();
		while(iter.hasNext()) {
			Arco<K,A> arco = iter.next();
			keys.add(arco.darVertice2());
		}
		
		return keys;
	}
	
	public Iterable<A> informacionArcos() {
		Queue<A> lista = new Queue<>();
		
		Iterator<K> iter = arcos.keys();
		while(iter.hasNext()) {
			Iterator<Arco<K,A>> iter2 = arcos.get(iter.next()).iterator();
			while(iter2.hasNext()) {
				lista.enqueue(iter2.next().darInfo());
			}
		}
		
		return lista;
	}
	
	public Iterable<Arco<K,A>> arcos() {
		Queue<Arco<K,A>> lista = new Queue<>();
		
		Iterator<K> iter = arcos.keys();
		while(iter.hasNext()) {
			Iterator<Arco<K,A>> iter2 = arcos.get(iter.next()).iterator();
			while(iter2.hasNext()) {
				lista.enqueue(iter2.next());
			}
		}
		
		return lista;
	}
	
	public Iterable<V> informacionVertices() {
		Queue<V> lista = new Queue<>();
		
		Iterator<K> iter = vertices.keys();
		while(iter.hasNext()) {
			V value = vertices.get(iter.next()).getValue();
			lista.enqueue(value);
		}
		
		return lista;
	}

	@Override
	public Iterator<K> vertices() {
		return vertices.keys();
	}

	@Override
	public Iterator<K> arcosKeys() {
		return arcos.keys();
	}
}
