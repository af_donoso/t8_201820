package data_structures;

import java.util.Iterator;

public class Queue<T> implements IQueue<T> 
{
	//-----------------------------------------------------------
	// Attributes
	//-----------------------------------------------------------

	private Node<T> firstNode, lastNode;

	private int listSize;

	//-----------------------------------------------------------
	// Constructor
	//-----------------------------------------------------------

	public Queue() {
		firstNode = null;
		lastNode = null;
	}

	//-----------------------------------------------------------
	// Methods
	//-----------------------------------------------------------

	@Override
	public Iterator<T> iterator() {

		return new Iterator<T>() {
			Node<T> act = null;

			@Override
			public boolean hasNext() {
				if (listSize == 0) {
					return false;
				}

				if (act == null) {
					return true;
				}

				return act.getNext() != null;
			}

			@Override
			public T next() {
				if (act == null) {
					act = firstNode;
				} else {
					act = act.getNext();
				}

				return act.getElement();
			}

		};
	}

	@Override
	public boolean isEmpty() {
		return firstNode == null;
	}

	@Override
	public int size() {
		return listSize;
	}

	@Override
	public void enqueue(T t) {
		Node<T> oldLast = lastNode;
		lastNode = new Node<>(t);

		if(isEmpty()) {
			firstNode = lastNode;
		} else {
			oldLast.setNext(lastNode);
		}

		listSize ++;
	}

	@Override
	public T dequeue() {
		if (!isEmpty()) {
			T element = firstNode.getElement();
			firstNode = firstNode.getNext();

			listSize --;

			return element;
		}

		return null;
	}
}
