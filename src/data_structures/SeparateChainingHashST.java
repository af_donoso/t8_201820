package data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;


public class SeparateChainingHashST <Key extends Comparable <Key>, value> implements ISeparateChainingHashST<Key, value>
{
	//-----------------------------------------------------------
	// Node class
	//-----------------------------------------------------------

	@SuppressWarnings("hiding")
	private class Node<Key, value>  
	{
		//-----------------------------------------------------------
		// Attributes
		//-----------------------------------------------------------

		private Key key;
		private value value;
		private Node<Key, value> next;

		//-----------------------------------------------------------
		// Constructores
		//-----------------------------------------------------------

		public Node()
		{

		}


		public Node(Key k, value value, Node siquiente)
		{
			this.key= k;
			this.value= value;
			this.next= siquiente;
		}

	}
	//-----------------------------------------------------------
	// Attributes
	//-----------------------------------------------------------

	private int n;
	private int m;
	private Node<Key, value>[] table;

	//-----------------------------------------------------------
	// Constructor
	//-----------------------------------------------------------


	public SeparateChainingHashST(int capacity)
	{
		this.m = capacity;
		this.n = 0;
		table = (Node<Key, value>[]) new Node[capacity];
		for (int i = 0; i < m; i++)
		{
			table[i] = (Node<Key, value>) new Node();
		}
	}
	//-----------------------------------------------------------
	// Metodos
	//-----------------------------------------------------------

	public boolean isEmpty() {
		return n == 0;
	}

	public int size() {
		return n;
	}

	private Node[] st = new Node[m]; 
	private int hash(Key key)
	{
		return (key.hashCode() & 0x7fffffff) % m; 
	}
	@Override
	public void put(Key k, value v)
	{
		if (k == null)
		{
			throw new IllegalArgumentException("called put() with key is null.");
		}
		if (v == null)
		{
			delete(k);
			return;
		}

		// double table size if average length of list >= 10
		if (n >= 5 * m)
			resize(2 * m);
		int i = hash(k);
		Node x = table[i];
		Node p = null;
		while (x != null) 
		{
			if (k.equals(x.key))
			{
				x.value = v;
				return;
			}
			p = x;
			x = x.next;
		}
		if (p == null) 
		{
			table[i] = new Node(k, v, null);
			n++;
		} 
		else 
		{
			p.next = new Node(k, v, null);
			n++;
		}

	}
	@Override
	public value get(Key k) 
	{
		int i = hash(k);
        Node x = table[i];
        while (x != null)
        {
            if (k.equals(x.key))
                return (value) x.value;
            x = x.next;
        }
        return null;
	}
	@Override
	public value delete(Key k)
	{	
		if (isEmpty())
		{
            throw new NoSuchElementException("called remove() with empty symbol table.");
		}
        if (get(k)==null)
        	
            return null;
        int i = hash(k);
        Node x = table[i];
        Node p = null;
        value oldValue = null;
        while (x != null) 
        {
            if (k.equals(x.key))
            {
                oldValue = (value) x.value;
                if (p == null)
                {
                    table[i] = x.next;
                } 
                else 
                {
                    p.next = x.next;
                }
                n--;
                break;
            }
            p = x;
            x = x.next;
        }

        // halve table size if average length of list <= 2
        if (m > 5 && n <= 2 * m)
            resize(m / 2);
        return oldValue;
	}
	@Override
	public Iterator<Key> keys() 
	{
		List<Key> list = new List<Key>();
    for (int i = 0; i < m; i++) 
    {
        Node<Key, value> x = table[i];
        while (x != null) 
        {
            if (x.key != null)
                list.add(x.key);
            x = x.next;
        }
    }
    return (Iterator<Key>) list.iterator();
	}

	private void resize(int capacity)
	{
		SeparateChainingHashST<Key, value> temp = new SeparateChainingHashST<Key, value>(capacity);
		for (int i = 0; i < m; i++) 
		{
			Node<Key, value> x = table[i];
			while (x != null) 
			{
				Key k = x.key;
				if (k != null)
					temp.put(k, this.get(k));
				x = x.next;
			}
		}
		this.m = temp.m;
		this.n = temp.n;
		this.table = temp.table;
	}
	
	public Node<Key, value> darRaiz(int i) {
		return table[i];
	}

}
