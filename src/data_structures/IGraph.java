package data_structures;

import java.util.Iterator;

public interface IGraph <K extends Comparable<K>, V, A>
{

	int V();
	int E();
	void addVertex( K idVertex, V infoVertex);
	void addEdge(K idVertexIni, K idVertexFin, A infoArc );
	V getInfoVertex(K idVertex);
	void setInfoVertex(K idVertex, V infoVertex);
	A getInfoArc(K idVertexIni, K idVertexFin);
	void setInfoArc(K idVertexIni, K idVertexFin, A infoArc);
	Iterable <K> adj (K idVertex);
	Iterator<K> vertices();
	Iterator<K> arcosKeys();
}
