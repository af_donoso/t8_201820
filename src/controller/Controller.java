package controller;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;

import data_structures.List;
import logic.Manager;

public class Controller {
	
	private static Manager manager = new Manager();
	
	public static void cargar() throws IOException {
		manager.cargar();
	}
	
	public static void cargarGrafo() throws FileNotFoundException, IOException, ParseException {
		manager.cargarGrafo();
	}
}
